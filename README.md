# ex-nginx

以下 WSL2 で行います。事前準備として:

-   docker desktop を WSL2 バックエンドでインストールし、起動しておく。
-   このリポジトリ <https://gitlab.com/nekonaq.ex/ex-nginx.git> を clone する。

まずはじめに (このディレクトリに置いてあるファイルとは関係なく) docker コマンドでコンテナを起動してみる。

    $ docker run -p 8080:80 nginx:stable

「nginx:stable」 というのはコンテナの元になる 「docker イメージ」 というもので、システムに存在しない場合は dockerhub というサイトからダウンロードしてきます。 dockerhub ( <https://hub.docker.com/> ) というのは、自作の docker イメージをだれでも公開できる場で、オープンソース・ソフトウェアの公式イメージも大抵ここにあります。 なお、このように docker image を置いておく場所をリポジトリと呼びます。 必要なら自前の docker イメージ・リポジトリを構築することもできます。

「nginx:stable」 は、dockerhub にある nginx 開発元の公式 docker イメージ・リポジトリ ( <https://hub.docker.com/_/nginx> ) に置いてある中から、 安定版 (stable version) のイメージを使ってね、という意味になります。

オプション「-p 8080:80」はポートのマッピングです。 ここでは外部からポート 8080 でアクセスしたらコンテナ内のポート 80 に接続しろという意味になります。

つまり上の docker run を実行した状態で、ブラウザで localhost:8080 にアクセスすると、コンテナ内で動作している nginx のポート 80 につながります。 ブラウザで「welcome to nginx!」の画面が観れたでしょうか。 ブラウザでアクセスするたびに docker run のログに nginx のアクセスログが表示される点も確認しておいてください。

次にもうひとつ WSL2 の端末を開いて docker ps とすると、実行中コンテナの状態が確認できまます。

    $ docker ps
    CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                  NAMES
    ccf3ba1f9ecd   nginx:stable   "/docker-entrypoint.…"   7 seconds ago   Up 6 seconds   0.0.0.0:8080->80/tcp   recursing_hypatia

これで実行中のコンテナの様子がわかります。 実行中のコンテナの ID は ccf3ba1f9ecd 、実行中のコンテナの名前はシステムが適当に付けた名前で recursing\_hypatia ということがわかります。 Ctrl-C で docker run の実行を終了することができます。

実行中コンテナを区別するために、固定の名前を付けたいことがあります。そのためにオプション &#x2013;name を使います。 ためしに「nginx-t」という名前にしてみましょう。

    $ docker run --name nginx-t -p 8080:80 nginx:stable

別の端末で:

    $ docker ps
    CONTAINER ID   IMAGE          COMMAND                  CREATED          STATUS          PORTS                  NAMES
    273e5649c9ce   nginx:stable   "/docker-entrypoint.…"   37 seconds ago   Up 36 seconds   0.0.0.0:8080->80/tcp   nginx-t

Ctrl-C で docker run の実行を終了し、もう一度同じことをやってみるとエラーになります。

    $ docker run --name nginx-t -p 8080:80 nginx:stable
    docker: Error response from daemon: Conflict. The container name "/nginx-t" is already in use by container "21c3..f34e". 
    You have to remove (or rename) that container to be able to reuse that name.
    See 'docker run --help'.

実は docker は終了したコンテナの情報を保持し続けます。先ほど終了した nginx-t が残っているので同じ名前のコンテナはつくれません、 というエラーです。

実行中および終了したすべてのコンテナを見るには docker ps にオプション -a を付けます。

    $ docker ps -a
    CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS                      PORTS     NAMES
    083f608cb0cd   nginx     "/docker-entrypoint.…"   9 seconds ago    Exited (0) 6 seconds ago              recursing_hypatia
    eb896ecf4bd3   nginx     "/docker-entrypoint.…"   27 seconds ago   Exited (0) 21 seconds ago             nginx-t

最初に起動&終了 recursing\_hypatia のイメージも残っていました。 docker rm で両方まとめて削除します。

    $ docker rm 083f608cb0cd eb896ecf4bd3
    083f608cb0cd
    eb896ecf4bd3
    
    $ docker ps -a
    CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

docker ps -a で確認するとすべて削除できていることがわかります。 docker rm でコンテナ ID を指定しましたが、コンテナ名でもうまくいきます。

ということで気を取り直して nginx-t を起動するわけですがその前に。 オプション &#x2013;rm を使うと終了したコンテナを自動的に削除してくれます。

    docker run --rm --name nginx-t -p 8080:80 nginx:stable

これで起動/終了するたびに docker rm する必要がなくなります。

コンテナというのは超軽量版の仮想環境だと考えてください。 コンテナ内で (nginx のような) アプリケーションが動作するための、最低限の環境 (オペレーティングシステム) です。 たとえば上の例で取り上げた nginx の docker イメージは Debian をベースにしています。

起動中コンテナの中で別のプログラムを実行する機能があるので bash を動かして確認していきましょう。 前記の docker run をした状態で docker exec コマンドで bash を動かします。

    $ docker exec -it nginx-t /bin/bash -i
    # cat /etc/os-release 
    ...略...
    # exit

これは起動中コンテナ nginx-t の中で bash を実行するという意味になります。 bash をバッチモードではなく対話モードで実行したいので「/bin/bash -i 」としています。 実行するプログラムは対話型のプログラムだということを示すために docker exec にオプション「-it」を指定しています。 この結果「#」プロンプトが表示されます。これはコンテナ nginx-t の中で動作している bash のプロンプトです。

Linux の種別を見るには一般的に `/etc/os-release` を確認するがの手っ取り早いです。 結果を見ると、このコンテナは Debian 11 をベースにしていることがわかります。

exit すると bash が終了し、それにより docker exec が終了します。

繰り返しになりますが、ようするに nginx の docker イメージは Debian がベースになっていて、 そこに nginx がインストールされている状態なわけです。

nginx がインストールされているということは、 設定ファイルなど nginx の動作に必要なファイルも (Debian のルールに従って) 存在するということです。 なので、これを確認してみます。

前記のように docker exec でコンテナ内で実行した bash で:

    # ls -la /etc/nginx
    total 48
    drwxr-xr-x 1 root root 4096 Jan 26 08:58 .
    drwxr-xr-x 1 root root 4096 Feb 14 11:12 ..
    drwxr-xr-x 1 root root 4096 Feb 14 11:12 conf.d
    -rw-r--r-- 1 root root 1007 Jan 25 15:03 fastcgi_params
    -rw-r--r-- 1 root root 5349 Jan 25 15:03 mime.types
    lrwxrwxrwx 1 root root   22 Jan 25 15:13 modules -> /usr/lib/nginx/modules
    -rw-r--r-- 1 root root  648 Jan 25 15:13 nginx.conf
    -rw-r--r-- 1 root root  636 Jan 25 15:03 scgi_params
    -rw-r--r-- 1 root root  664 Jan 25 15:03 uwsgi_params
    
    # ls -la /usr/share/nginx/html
    total 16
    drwxr-xr-x 2 root root 4096 Jan 26 08:58 .
    drwxr-xr-x 3 root root 4096 Jan 26 08:58 ..
    -rw-r--r-- 1 root root  497 Jan 25 15:03 50x.html
    -rw-r--r-- 1 root root  615 Jan 25 15:03 index.html
    
    # exit

コンテナ内のディレクトリ `/etc/nginx` の下には nginx の設定ファイルがあります。 コンテナ内のディレクトリ `/usr/share/nginx/html` にはデフォルトの HTML ファイルがあります。 docker run してブラウザで見たときのドキュメント・ルートはここになります。 つまり「welcome to nginx!」を表示しているのはここにある index.html によるものです。

ということは `/usr/share/nginx/html` を差し替えれば自分の好きなコンテンツを提供できます。 ためしてみましょう。

最初に git clone したディレクトリ ex-nginx がカレントディレクトリであることを確認します。次に:

    $ docker run --rm --name nginx-t -v $PWD/html:/usr/share/nginx/html:ro -p 8080:80 nginx:stable

オプション -p はポートのマッピングでしたが、オプション -v はボリューム (フォルダ) のマッピングです。 上記は、コンテナ内の `/usr/share/nginx/html` に対して、 カレントディレクトリにある html というディレクトリをリードオンリー (ro) でマッピングするという意味になります。

これでブラウザを使って <http://localhost:8080> をアクセスすると、 ディレクトリ html の下にある index.html の内容、つまり「Hi, there」というのが表示されます。

コンテナ内のディレクトリ `/etc/nginx` も同様にマッピングして好みの設定に変更できますが、ここでは触れません。

ここまでの演習でわかったこと:

-   docker をインストールして docker イメージを持ってくれば システムにアプリケーションを直接インストールする必要がなくなる。
-   コンテナが公開するポート番号やコンテナが使う設定ファイルなどは必要に応じて自由に変更できる。

コンテナを使うことで、たとえば「システムのバージョンがこれだから、Java VM はこのバージョンでないとインストールできない」とか 「このアプリケーションを動かすには システムにこれこれのライブラリをインストールしておくこと」などの制約に縛られることが少なくなり、 システム構築運用のコストを下げることができます。

さて、docker を使うにあたって、先の演習のように起動のたびにあれこれオプションを指定するのは大変です。 そこで、これらのオプションを定義ファイルに書いておき、しかも関連する複数コンテナをまとめて管理するようにしたのが docker-compose です。

ex-nginx をカレントディレクトリにして、以下をタイプします。

    docker-compose up

これだけです。先ほどの docker run と同じく localhost:8080 で「Hi, there」と表示されるのが確認できます。 docker-compose の設定は、ファイル docker-compose.yml に書いてあります。 docker イメージの指定や、オプション -p やオプション -v に相当する定義をしているのがわかると思います。

docker-compose ps で起動しているサービスの状態がわかります。

    $ docker-compose ps
          Name                    Command               State          Ports        
    --------------------------------------------------------------------------------
    ex-nginx_nginx_1   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp
    
    $ docker ps
    CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS              PORTS                  NAMES
    8b5a472ca4aa   nginx:stable   "/docker-entrypoint.…"   4 minutes ago   Up About a minute   0.0.0.0:8080->80/tcp   ex-nginx_nginx_1

docker-compose は docker を使っているので、もちろん docker ps でも状態がわかります。 docker ps はシステム全体の docker コンテナの状態を表示します。 docker-compose ps は、カレントディレクトリにある docker-compose.yml に関連するサービスの状態だけを表示します。

docker-compose up にオプション -d を付けるとバックグラウンドでコンテナを起動します。 このときコンテナの実行ログをコンソールに表示しないので、ログを見るには docker-compose logs を使います。 コンテナを終了するには docker-compose down を使います。

以上を続けてやってみます。 docker-compose logs のオプション -f というのは、ログ表示後に終了せず、 ログが追加されたら次々と表示するという意味 (tail -f と同じ) です。 Ctrl-C で終了できます。

    $ docker-compose up -d
    Creating network "ex-nginx_default" with the default driver
    Creating ex-nginx_nginx_1 ... 
    
    $ docker-compose ps
          Name                    Command               State          Ports        
    --------------------------------------------------------------------------------
    ex-nginx_nginx_1   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8080->80/tcp
    
    $ docker-compose logs -f
    Attaching to ex-nginx_nginx_1
    nginx_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
    nginx_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
    ...略
    nginx_1  | 172.20.0.1 - - [25/Feb/2022:13:12:18 +0000] "GET / HTTP/1.1" 200 110 "-" "Mozilla/5.0...
    ...略
    
    $ docker-compose down
    Stopping ex-nginx_nginx_1 ... 
    Removing ex-nginx_nginx_1 ... 
    Removing network ex-nginx_default
    
    $ docker-compose ps
    Name   Command   State   Ports
    ------------------------------

ここまでの演習でわかったことは: システムに web サーバーやらアプリケーションやらあれこれインストールしなくても、 docker + docker-compose が動作する環境で docker-compose.yml さえあればコマンド一発でサービスが開始できる。

もし余裕があれば、virtualbox ゲストや EC2 インスタンス、あるいは digitalocean droplet でもなんでもかまいませんので リポジトリ <https://gitlab.com/nekonaq.ex/ex-nginx.git> を clone して docker-compose up してみてください。 まったく同じように動作するはずです。

ここでひとつおまけです。 docker + docker-compose をインストールするスクリプトを用意しています。(ubuntu 限定)

    curl -fsSL https://bit.ly/install-docker-ce |/bin/sh /dev/stdin --with-compose

この演習シリーズの最初のほうでやったようにdocker + docker-compose を手でインストールしてもらいました。 このスクリプトを使えばかなり作業時間が節約できるのがわかると思います。

ということで、ここで紹介した内容に加えて自作のアプリケーションをコンテナ化するスキルがあれば、

1.  install-docker-ce スクリプトで docker + docker-compose をインストールする。
2.  docker-compose.yml を含むリポジトリを clone する
3.  docker-compose up -d する

これだけで好きなプラットフォームで自作のサービスが動くわけです。 もちろん事前準備とか、若干の定義ファイルの調整は必要になるかと思いますが。

インストールの作業時間も短縮できるし、サービスのインストール手順書もかなり短くて済みますよね。